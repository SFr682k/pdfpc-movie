%\iffalse
%  Doc-Source file to use with LaTeX2e
%  Copyright (C) 2018 Sebastian Friedl
%
%  This work is subject to the LaTeX Project Public License, Version 1.3c or -- at
%  your option -- any later version of this license.
%  This work consists of the files pdfpc-movie.dtx, pdfpc-movie.ins, pdfpc-movie-doc.dtx
%  and the derived file pdfpc-movie.sty.
%
%  This work has the LPPL maintenance status 'maintained'.
%  Current maintainer of the work is Sebastian Friedl.
%
%  -------------------------------------------------------------------------------------------
%
%  Provides a \pdfpcmovie command for linking movies in a way compatible to the
%  PDF Presenter Console.
%
%  -------------------------------------------------------------------------------------------
%
%  Please report bugs and other problems as well as suggestions for improvements
%  to my email address (sfr682k@t-online.de).
%
%  -------------------------------------------------------------------------------------------
%\fi

% !TeX spellcheck=en_US


\documentclass[11pt]{ltxdoc}

\usepackage{iftex}
\RequireLuaTeX

\usepackage[no-math]{fontspec}
\usepackage{polyglossia}
\setdefaultlanguage{english}
\usepackage[english]{selnolig}

\usepackage{array}
\usepackage{csquotes}
\usepackage{hologo}
\usepackage[unicode, pdfborder={0 0 0}, linktoc=all, hyperindex=false]{hyperref}
\usepackage{bookmark}


\parindent0pt

\usepackage[erewhon]{newtxmath}
\setmainfont{erewhon}
\setsansfont[Scale=MatchLowercase]{Source Sans Pro}
\setmonofont[Scale=MatchLowercase]{Hack}

\usepackage[a4paper,left=4.50cm,right=2.75cm,top=3.25cm,bottom=2.75cm,nohead]{geometry}


\hyphenation{}


\MakeShortVerb{"}
\CheckSum{0}


\newcommand*{\sty}[1]{\textsf{#1}}

\def\param#1{\textit{\rmfamily\mdseries\ensuremath{\langle}#1\ensuremath{\rangle}}}
    

\RecordChanges




\title{The \sty{pdfpc-movie} package \\ {\large\url{https://gitlab.com/SFr682k/pdfpc-movie}}}
\author{Sebastian Friedl \\ \href{mailto:sfr682k@t-online.de}{\ttfamily sfr682k@t-online.de}}
\date{2018/07/21 (v1.0)}

\hypersetup{pdftitle={The pdfpc-movie package}, pdfauthor={Sebastian Friedl}}

\begin{document}
    \maketitle
    \thispagestyle{empty}
    
    
    \medskip
    \begin{abstract}
        \noindent%
        This package provides a "\pdfpcmovie" command for hyperlinking movies in a way compatible to the \href{http://pdfpc.github.io/}{PDF Presenter Console (\texttt{pdfpc}), a GPL2 licensed multi-monitor PDF presentation viewer application available on GitHub}\footnote{\url{http://pdfpc.github.io/}}.
    \end{abstract}
    
    
    \tableofcontents
    
    
    
    
    
    \clearpage
    \subsection*{Dependencies and other requirements}
    \addcontentsline{toc}{subsection}{Dependencies and other requirements}
    \sty{pdfpc-movie} requires the use of \LaTeXe. \\
    It depends on the following packages:
    \begin{multicols}{3}\sffamily\centering
        etoolbox \\ hyperref \\ pgfkeys
    \end{multicols}

    
    
    \subsection*{Installation}
    \addcontentsline{toc}{subsection}{Installation}
    Extract the \textit{package} file first:
    \begin{enumerate}
        \item Run \LaTeX\ over the file "pdfpc-movie.ins"
        \item Move the resulting ".sty" file to "TEXMF/tex/latex/pdfpc-movie/"
    \end{enumerate}
    
    Then, you can compile the \textit{documentation} yourself by executing \\[\smallskipamount]
    "lualatex pdfpc-movie-doc.dtx" \\
    "makeindex -s gind.ist pdfpc-movie-doc.idx" \\
    "makeindex -s gglo.ist -o pdfpc-movie-doc.gls pdfpc-movie-doc.glo" \\
    "lualatex pdfpc-movie-doc.dtx" \\
    "lualatex pdfpc-movie-doc.dtx"
    
    \smallskip
    or just use the precompiled documentation shipped with the source files. \\
    In both cases, copy the files "pdfpc-movie-doc.pdf" and "README.md" to \\
    "TEXMF/doc/latex/pdfpc-movie/"
    
    
    
    \subsection*{License}
    \addcontentsline{toc}{subsection}{License}
    \textcopyright\ 2018 Sebastian Friedl
    
    \smallskip
    This work may be distributed and/or modified under the conditions of the \LaTeX\ Project Public License, either version 1.3c of this license or (at your option) any later version.
    
    \smallskip
    The latest version of this license is available at \url{http://www.latex-project.org/lppl.txt} and version 1.3c or later is part of all distributions of \LaTeX\ version 2008-05-04 or later.
    
    \smallskip
    This work has the LPPL maintenace status \enquote*{maintained}. \\
    Current maintainer of this work is Sebastian Friedl.
    
    \medskip
    This work consists of the following files:
    \begin{itemize}\itemsep 0pt
        \item "pdfpc-movie.dtx",
        \item "pdfpc-movie.ins",
        \item "pdfpc-movie-doc.dtx" and
        \item the derived file "pdfpc-movie.sty".
    \end{itemize}





    % DOCUMENTATION PART ----------------------------------------------------------------------
    \clearpage
    \part{The documentation}
    \section*{Loading \sty{pdfpc-movie}}
    \addcontentsline{toc}{subsection}{Loading \sty{pdfpc-movie}}
    Load \sty{pdfpc-movie} as any other package by adding "\usepackage{pdfpc-movie}" to your preamble.
    There are no package options available.
    
    
    \section*{Including movie files}
    \addcontentsline{toc}{subsection}{Including movie files}
    The package provides a "\pdfpcmovie" command using the syntax
    \begin{center}\DescribeMacro{\pdfpcmovie}%
        "\pdfpcmovie["\param{options}"]{"\param{poster material}"}{"\param{path to movie file}"}"
    \end{center}
    
    It creates a hyperlink to a movie with \param{path to movie file} being the path of the movie file relative to the PDF it is linked from. These hyperlinks are evaluated by "pdfpc", so that the movie (usually) starts playing when clicking onto \param{poster material}. \\
    It will \emph{not} embed the movie into the PDF file in the sense that it is part of the "foo.pdf" file. Hence, the file must be copied and passed along with the PDF file in a manner allowing the viewer application to find the file at \param{path to movie file}. \\
    As a recommendation, \param{path to movie file} should \emph{not} contain any \enquote{special} characters or spaces.
    
    \bigskip
    The \param{poster material} is placed inside a "\hbox" for determining and changing the space occupied by the poster material; therefore, it is not possible to insert any line breaks or similar \LaTeX\ code not allowed inside "\hbox"es. \\
    The movie will playback in the area taken by the \param{poster material} or set via the "width"/"height" keys. Using a frame of the movie as \param{poster material} will ensure the correct aspect ratio.
    
    \bigskip
    The following \param{options} may be given as comma-separated list:
    \begin{description}\itemsep0pt
        \item[\texttt{width=\param{\TeX\ dimension}}]%
            Overrides the width of \param{poster material} and sets it to the given \param{\TeX~dimension}
        
        \item[\texttt{height=\param{\TeX\ dimension}}]%
            Overrides the height of \param{poster material} and sets it to the given \param{\TeX~dimension}
        
        \item[\texttt{depth=\param{\TeX\ dimension}}]%
            Overrides the depth of \param{poster material} and sets it to the given \param{\TeX~dimension}
        
        \item[\texttt{autostart}]%
            Causes the movie to start playing immediately when the page is shown. \\[\smallskipamount]
            A test with version 4.1.2 of "pdfpc" lead to the result that it is possible to autostart multiple movies placed on the same page; however, it turned out that they didn't play synchronously, but slightly \enquote{shifted} (by approximately 0.25\,s).
            
        \item[\texttt{loop}]
            Let the movie start again when the end has been reached. \\
            Normally, the movie just stops there.
        
        \item[\texttt{noprogress}]
            Instructs "pdfpc" to avoid showing the progress bar below the movie. \\
            As of "pdfpc" version 4.1.2, the progress bar will still be shown when the cursor is moved across the movie's bottom margin.
        
        \item[\texttt{start=\param{time}}]
            Makes the movie start at \param{time}~seconds. \\
            \param{time} has to be an integer value without suffixes. For example, "start=2" is valid, while "start=4.2" and "start=1s" aren't.
        
        \item[\texttt{stop=\param{time}}]
            Makes the movie stop (and eventually restart) when the playback position has reached \param{time}~seconds. \\
            As above, \param{time} has to be an integer value without suffixes.
    \end{description}
    
    If you don't need any options, you may omit the optional "["\param{options}"]" parameter.
    
    
    \subsubsection*{Examples}
    "\pdfpcmovie{\includegraphics[width=\textwidth]{foo}}{foo.mp4}"
    
    {\leftskip2.5em%
        Creates a link to the movie "foo.mp4", using an image as poster material. \\
        The poster image will be replaced by the movie when clicking it.
    \par}
    
    
    \medskip
    "\pdfpcmovie[autostart, width=8cm, height=4.5cm]{Restart}{bar.avi}"
    
    {\leftskip2.5em%
        Creates a link to the movie "bar.avi". \\
        The movie is started as soon as the page is displayed and takes an area of 8\,cm width and 4.5\,cm height. \\
        The movie stops after playing.
    \par}


    \medskip
    "\pdfpcmovie[autostart, start=2, stop=5, loop]%" \\
    "  {\includegraphics[height=3cm]{res/baz}}{res/baz.mov}"
    
    {\leftskip2.5em%
        Creates a link to the movie "baz.mov" residing in the sub-directory "res/". \\
        As soon as the page is displayed, the movie starts at 2~seconds and restarts when the playback position has reached 5~seconds.
    \par}
    
    
    
    
    
    % Start determining the checksum from here
    \StopEventually{%
        \clearpage
        \PrintChanges}
    \clearpage
    
    
    
    
    \part{The package code}
    \CodelineNumbered
    \DocInput{pdfpc-movie.dtx}
    
    
    \Finale
\end{document}

